#include <iostream>

#include <idg.h>
#include <idg-fft.h>

#include <fftw3.h>

#include "helper.h"

void fft2f(unsigned batch, int m, int n, std::complex<float> *data)
{
  fftwf_complex *tmp = reinterpret_cast<fftwf_complex*>(data);

  fftwf_plan_with_nthreads(omp_get_max_threads());
  fftwf_plan plan = fftwf_plan_dft_2d(m, n, tmp, tmp, FFTW_FORWARD, FFTW_FLAGS);
  LIKWID_MARKER_REGISTER("fft");
  LIKWID_MARKER_START("fft");
  for (size_t i = 0; i < batch; i++) {
    tmp = reinterpret_cast<fftwf_complex *>(&data[i * m * n]);
    fftwf_execute_dft(plan, tmp, tmp);
  }
  LIKWID_MARKER_STOP("fft");
}

void fft2f_embed(unsigned batch, int height, int width, int width_padded, std::complex<float> *data)
{
  fftwf_complex *tmp = reinterpret_cast<fftwf_complex*>(data);
  fftwf_plan_with_nthreads(omp_get_max_threads());
	int rank = 2;
	int dim[2] = {height, width};
	int howmany = batch;
	int inembed[2] = {height, width_padded};
	int istride = 1;
	int idist = height * width_padded;
	int *onembed = inembed;
	int ostride = 1;
	int odist = idist;
	int sign = FFTW_FORWARD;
	unsigned flags = FFTW_FLAGS;
	fftwf_plan plan = fftwf_plan_many_dft(
														 rank, dim, howmany,
                             tmp, inembed,
                             istride, idist,
                             tmp, onembed,
                             ostride, odist,
                             sign, flags);
  LIKWID_MARKER_REGISTER("fft-embed");
  LIKWID_MARKER_START("fft-embed");
	fftwf_execute_dft(plan, tmp, tmp);
  LIKWID_MARKER_STOP("fft-embed");
}

void fft2f_oplace(unsigned batch, int m, int n, std::complex<float> *in, std::complex<float> *out)
{
  fftwf_complex *in_ptr = reinterpret_cast<fftwf_complex*>(in);
  fftwf_complex *out_ptr = reinterpret_cast<fftwf_complex*>(out);

  fftwf_plan_with_nthreads(omp_get_max_threads());
  fftwf_plan plan = fftwf_plan_dft_2d(m, n, in_ptr, out_ptr, FFTW_FORWARD, FFTW_FLAGS);
  LIKWID_MARKER_REGISTER("fft-oplace");
  LIKWID_MARKER_START("fft-oplace");
  for (unsigned i = 0; i < batch; i++) {
    uint64_t offset = size_t(i) * size_t(m) * size_t(n);
    fftwf_execute_dft(plan, in_ptr + offset, out_ptr + offset);
  }
  LIKWID_MARKER_STOP("fft-oplace");
}

void fft2f_composite(unsigned batch, int m, int n, std::complex<float> *data)
{
  fftwf_complex *in_ptr = reinterpret_cast<fftwf_complex*>(data);
  fftwf_complex *out_ptr = reinterpret_cast<fftwf_complex*>(data);

  fftwf_plan_with_nthreads(1);

  fftwf_plan plan_col = fftwf_plan_dft_1d(m, nullptr, nullptr, FFTW_FORWARD, FFTW_FLAGS);
  fftwf_plan plan_row = fftwf_plan_dft_1d(n, nullptr, nullptr, FFTW_FORWARD, FFTW_FLAGS);

  LIKWID_MARKER_REGISTER("fft-composite");
  LIKWID_MARKER_START("fft-composite");
  for (unsigned i = 0; i < batch; i++) {
    // FFT over rows
    #pragma omp parallel for
    for (size_t y = 0; y < m; y++) {
      size_t offset = i * m * n + y * n;
      fftwf_execute_dft(plan_row, in_ptr + offset, out_ptr + offset);
    }

    // FFT over columns
    int unroll = 4;
    #pragma omp parallel for
    for (size_t x = 0; x < n; x += unroll) {
      std::complex<float> tmp[m*unroll];

      // Copy column into temporary buffer
      for (size_t y = 0; y < m; y++) {
        for (size_t j = 0; j < unroll; j++) {
          if ((x + j) < n) {
            tmp[j * m + y] = data[i * m * n + y * n + x + j];
          }
        }
      }

      // Perform the FFT
      for (size_t j = 0; j < unroll; j++) {
        fftwf_complex *tmp_ptr = reinterpret_cast<fftwf_complex*>(&tmp[j * m]);
        fftwf_execute_dft(plan_col, tmp_ptr, tmp_ptr);
      }

      // Store the result in the output buffer
      for (size_t y = 0; y < m; y++) {
        for (size_t j = 0; j < unroll; j++) {
          if ((x + j) < n) {
            data[i * m * n + y * n + x + j] = tmp[j * m + y];
          }
        }
      }
    }
  }
  LIKWID_MARKER_STOP("fft-composite");
}

void init_grid(idg::Grid& grid)
{
  for (int z = 0; z < grid.get_z_dim(); z++)
  {
    for (int y = 0; y < grid.get_y_dim(); y++)
    {
      for (int x = 0; x < grid.get_x_dim(); x++)
      {
          float real = (float) (1+z+y) / grid.get_y_dim();
          float imag = (float) (1+z+x) / grid.get_x_dim();
          grid(0, z, y, x) = std::complex<float>(real, imag);
      }
    }
  }
}

int main(int argc, char **argv)
{
  int grid_size = SIZE;
  int nr_polarizations = 1;
  
  idg::Grid grid(1, nr_polarizations, grid_size, grid_size);
  idg::Grid grid_ref(1, nr_polarizations, grid_size, grid_size);
  init_grid(grid);
  init_grid(grid_ref);
  std::cout << "grid size: " << grid_size << ", " << grid.bytes() << " Bytes" << std::endl;
  
  LIKWID_MARKER_INIT;

  #if 1
  // Reference
  std::cout << "fft" << std::endl;
  fft2f(nr_polarizations, grid_size, grid_size, grid_ref.data());
  #endif

  #if 1
  // Out-of-place
  idg::Grid grid_oplace(1, nr_polarizations, grid_size, grid_size);
  grid_oplace.zero();
  std::cout << "fft-oplace" << std::endl;
  fft2f_oplace(nr_polarizations, grid_size, grid_size, grid.data(), grid_oplace.data());
  get_accuracy(grid_ref, grid_oplace);
  std::cout << std::endl;
  #endif

  #if 1
  // Composite
  idg::Grid grid_composite(1, nr_polarizations, grid_size, grid_size);
  init_grid(grid_composite);
  std::cout << "fft-composite" << std::endl;
  fft2f_composite(nr_polarizations, grid_size, grid_size, grid_composite.data());
  get_accuracy(grid_ref, grid_composite);
  std::cout << std::endl;
  #endif

  #if 1
  // Embedding
  int padded_size = grid_size + 16;
  idg::Grid grid_embed(1, nr_polarizations, grid_size, padded_size);
  for (int z = 0; z < grid.get_z_dim(); z++)
  {
    #pragma omp parallel for
    for (int y = 0; y < grid.get_y_dim(); y++)
    {
      for (int x = 0; x < grid.get_x_dim(); x++)
      {
          grid_embed(0, z, y, x) = grid(0, z, y, x);
      }
    }
  }
  std::cout << "fft-embed" << std::endl;
  fft2f_embed(nr_polarizations, grid_size, grid_size, padded_size, grid_embed.data());
  for (int z = 0; z < grid.get_z_dim(); z++)
  {
    #pragma omp parallel for
    for (int y = 0; y < grid.get_y_dim(); y++)
    {
      for (int x = 0; x < grid.get_x_dim(); x++)
      {
          grid(0, z, y, x) = grid_embed(0, z, y, x);
      }
    }
  }
  get_accuracy(grid_ref, grid);
  std::cout << std::endl;
  #endif

  std::cout << "idg::ifft" << std::endl;
  LIKWID_MARKER_REGISTER("fft-idg");
  LIKWID_MARKER_START("fft-idg");
  idg::ifft2f(nr_polarizations, grid_size, grid_size, grid.data(0, 0, 0, 0));
  LIKWID_MARKER_STOP("fft-idg");
  std::cout << std::endl;

  LIKWID_MARKER_CLOSE;
}
