#include <fftw3.h>
#include <idg.h>

#include "helper.h"

void fft2f_c2r(int imgHeight, int imgWidth, std::complex<float> *in, float *out)
{
  const size_t imgSize = imgWidth * imgHeight;
  const size_t complexWidth = imgWidth / 2 + 1;
  const size_t complexSize = complexWidth * imgHeight;

  fftwf_complex* temp = fftwf_alloc_complex(complexSize);
  fftwf_complex* in_ptr = reinterpret_cast<fftwf_complex*>(in);
  memset(temp, 0, complexSize * sizeof(fftwf_complex));

  fftwf_plan_with_nthreads(omp_get_max_threads());
  fftwf_plan plan_c2r = fftwf_plan_dft_c2r_2d(imgHeight, imgWidth, nullptr, nullptr, FFTW_FLAGS);

  LIKWID_MARKER_REGISTER("fft");
  LIKWID_MARKER_START("fft");
  fftwf_execute_dft_c2r(plan_c2r, in_ptr, reinterpret_cast<float*>(temp));

  float fact = 1.0 / imgSize;
  for (size_t i = 0; i != complexSize; ++i)
    out[i] = reinterpret_cast<float*>(temp)[i] * fact;

  LIKWID_MARKER_STOP("fft");

  fftwf_free(temp);
  fftwf_destroy_plan(plan_c2r);
  fftwf_cleanup_threads();
}

void fft2f_c2r_composite(int imgHeight, int imgWidth, std::complex<float> *in, float *out, bool scale = true)
{
  const size_t imgSize = imgWidth * imgHeight;
  const size_t complexWidth = imgWidth / 2 + 1;
  const size_t complexHeight = imgHeight / 2 + 1;

  std::vector<std::complex<float>> temp1(imgHeight * complexWidth);

  fftwf_plan_with_nthreads(1);
  fftwf_plan plan_c2c_col = fftwf_plan_dft_1d(imgHeight, nullptr, nullptr, FFTW_BACKWARD, FFTW_FLAGS);
  fftwf_plan plan_c2r_row = fftwf_plan_dft_c2r_1d(imgWidth, nullptr, nullptr, FFTW_FLAGS);

  LIKWID_MARKER_REGISTER("fft-composite");
  LIKWID_MARKER_START("fft-composite");

  #pragma omp parallel for
  for (size_t x = 0; x < complexWidth; x++)
  {
    for (size_t y = 0; y < imgHeight; y++)
    {
      temp1[x * imgHeight + y] = in[y * complexWidth + x];
    }
      
    fftwf_complex* temp1_ptr = reinterpret_cast<fftwf_complex*>(
      &temp1[x * imgWidth]);
    fftwf_execute_dft(plan_c2c_col, temp1_ptr, temp1_ptr);
  }

  #pragma omp parallel for
  for (size_t y = 0; y < complexHeight; y++)
  {
    memset(&out[y * imgWidth], 0, imgWidth * sizeof(float));
  }

  #pragma omp parallel for
  for (size_t y = 0; y < complexHeight; y++)
  {
    std::vector<std::complex<float>> temp2(complexWidth);

    for (size_t x = 0; x < complexWidth; x++)
    {
      temp2[x] = temp1[x * imgHeight + y];
    }

    fftwf_execute_dft_c2r(plan_c2r_row, reinterpret_cast<fftwf_complex*>(temp2.data()), reinterpret_cast<float*>(temp2.data()));

    if (scale)
    {
      for (size_t x = 0; x < complexWidth; x++) {
        float fact = 1.0f / imgSize;
        temp2[x] *= fact;
      }
    }

    if (y == 0) {
      memcpy(&out[y * imgWidth], temp2.data(), imgWidth * sizeof(float));
    } else {
      int rowWidth;
      if (imgWidth % 2 == 0)
      {
        rowWidth = imgWidth - (2*y);
      } else {
        rowWidth = imgWidth - y;
      }
      int rowStart = imgWidth - rowWidth;
      for (size_t x = 0; x < rowWidth; x++)
      {
        int x1 = rowStart + x;
        out[y * imgWidth + x1] = reinterpret_cast<float*>(temp2.data())[x];
      }

      if ((y < (imgHeight / 2)) && (imgSize % 2 == 0))
      {
        for (size_t x = 0; x < y; x++)
        {
          int y1 = y + 1;
          int x1 = 2*x + 1;
          int x2 = (complexWidth - y - 1)+ x;
          out[y1 * imgWidth + x1] = temp2[x2].imag();
        }
      }
    }
  }

  if (imgSize % 2 == 1)
  {
    #pragma omp parallel for
    for (int y = 0; y < imgHeight; y++)
    {
      for (int x = 0; x < (imgWidth/2) - y; x++)
      {
        int y1 = y + x + 1;
        int y2 = y1 - 1;
        int x1 = y - 1;
        int x2 = x1+2 + 2*x;
        out[size_t(y1) * imgWidth + x1] = -out[size_t(y2) * imgWidth + x2];
      }
    }
  }

  LIKWID_MARKER_STOP("fft-composite");

  fftwf_destroy_plan(plan_c2c_col);
  fftwf_destroy_plan(plan_c2r_row);
  fftwf_cleanup_threads();
}

void test_c2r(int size)
{
  int size2 = size / 2 + 1;
  std::cout << "size: " << size << ", complex size: " << size2 << std::endl;
  
  std::cout << "input" << std::endl;
  idg::Array2D<std::complex<float>> in(size, size2);
  init_data(in);
  
  // Reference
  std::cout << "fft2f_c2r" << std::endl;
  idg::Array2D<float> out_reference(size2, size);
  out_reference.zero();
  fft2f_c2r(size, size, in.data(), out_reference.data());
  print(size2, size, out_reference.data());

  // Composite
  init_data(in);
  std::cout << "fft2f_c2r_composite" << std::endl;
  idg::Array2D<float> out_composite(size2, size);
  out_composite.zero();
  fft2f_c2r_composite(size, size, in.data(), out_composite.data());
  print(size2, size, out_composite.data());

  get_accuracy(out_reference, out_composite);
  std::cout << std::endl;
}

int main(int argc, char **argv)
{
  LIKWID_MARKER_INIT;

  test_c2r(SIZE);
  test_c2r(SIZE*2);
  test_c2r(SIZE+1);

  LIKWID_MARKER_CLOSE;
}
