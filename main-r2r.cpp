#include <iostream>
#include <complex>

#include <idg.h>

#include <fftw3.h>

#include "helper.h"


/********************************************************************************
 * R2C
 ********************************************************************************/
void fft2f_r2c_composite(int imgHeight, int imgWidth, float *in, fftwf_complex *out, fftwf_complex *temp = nullptr)
{
  assert(imgHeight == imgWidth);
  const size_t imgSize = imgWidth * imgHeight;
  const size_t complexWidth = imgWidth / 2 + 1;
  const size_t complexSize = imgHeight * complexWidth;

  fftwf_complex* temp1 = temp ? temp : fftwf_alloc_complex(complexSize);

  fftwf_plan_with_nthreads(1);
  fftwf_plan plan_r2c_row = fftwf_plan_dft_r2c_1d(imgWidth, nullptr, nullptr, FFTW_ESTIMATE);
  fftwf_plan plan_c2c_col = fftwf_plan_dft_1d(imgHeight, nullptr, nullptr, FFTW_FORWARD, FFTW_ESTIMATE);

  LIKWID_MARKER_START("fft-composite");

  #pragma omp parallel for
  for (size_t y = 0; y < imgHeight; y++)
  {
    float* temp2 = fftwf_alloc_real(imgWidth);
    memcpy(temp2, &in[y * imgWidth], imgWidth * sizeof(float));
    fftwf_execute_dft_r2c(plan_r2c_row, temp2, &temp1[y * complexWidth]);
    fftwf_free(temp2);
  }

  #pragma omp parallel for
  for (size_t x = 0; x < complexWidth; x++)
  {
    fftwf_complex* temp2 = fftwf_alloc_complex(imgHeight);

    // Copy input
    for (size_t y = 0; y < imgHeight; y++)
    {
      memcpy(&temp2[y], &temp1[y * complexWidth + x], sizeof(fftwf_complex));
    }

    // Perform 1D FFT over column
    fftwf_execute_dft(plan_c2c_col, temp2, temp2);

    // Transpose output
    for (size_t y = 0; y < imgHeight; y++)
    {
      memcpy(&out[y * complexWidth + x], temp2[y], sizeof(fftwf_complex));
    }

    fftwf_free(temp2);
  }

  LIKWID_MARKER_STOP("fft-composite");

  fftwf_destroy_plan(plan_r2c_row);
  fftwf_destroy_plan(plan_c2c_col);
  if (!temp) {
    fftwf_free(temp1);
  }
  fftwf_cleanup_threads();
}


/********************************************************************************
 * C2R
 ********************************************************************************/
void fft2f_c2r_composite(int imgHeight, int imgWidth, fftwf_complex *in, float *out, fftwf_complex *temp = nullptr)
{
  const size_t imgSize = imgWidth * imgHeight;
  const size_t complexWidth = imgWidth / 2 + 1;
  const size_t complexSize = imgHeight * complexWidth;

  fftwf_complex* temp1 = temp ? temp : fftwf_alloc_complex(complexSize);

  fftwf_plan_with_nthreads(1);
  fftwf_plan plan_c2c_col = fftwf_plan_dft_1d(imgHeight, nullptr, nullptr, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftwf_plan plan_c2r_row = fftwf_plan_dft_c2r_1d(imgWidth, nullptr, nullptr, FFTW_ESTIMATE);

  LIKWID_MARKER_START("fft-composite");

  #pragma omp parallel for
  for (size_t x = 0; x < complexWidth; x++)
  {
    // Transpose input
    for (size_t y = 0; y < imgHeight; y++)
    {
      memcpy(&temp1[x * imgHeight + y], &in[y * complexWidth + x], sizeof(fftwf_complex));
    }
      
    // Perform 1D C2C FFT over column
    fftwf_complex* temp1_ptr = &temp1[x * imgHeight];
    fftwf_execute_dft(plan_c2c_col, temp1_ptr, temp1_ptr);
  }

  #pragma omp parallel for
  for (size_t y = 0; y < imgHeight; y++)
  {
    fftwf_complex* temp2 = fftwf_alloc_complex(complexWidth);

    // Transpose input
    for (size_t x = 0; x < complexWidth; x++)
    {
      memcpy(&temp2[x], &temp1[x * imgHeight + y], sizeof(fftwf_complex));
    }

    // Perform 1D C2R FFT over row
    fftwf_execute_dft_c2r(plan_c2r_row, temp2, reinterpret_cast<float*>(temp2));

    // Copy output
    memcpy(&out[y * imgWidth], temp2, imgWidth * sizeof(float));

    fftwf_free(temp2);
  }

  LIKWID_MARKER_STOP("fft-composite");

  fftwf_destroy_plan(plan_c2c_col);
  fftwf_destroy_plan(plan_c2r_row);
  if (!temp)
  {
    fftwf_free(temp1);
  }
  fftwf_cleanup_threads();
}

/********************************************************************************
 * R2R
 ********************************************************************************/
void fft2f_r2r(int imgHeight, int imgWidth, float *data)
{
  const size_t imgSize = imgWidth * imgHeight;
  const size_t complexWidth = imgWidth / 2 + 1;
  const size_t complexSize = imgHeight * complexWidth;

  fftwf_complex* temp = fftwf_alloc_complex(complexSize);

  fftwf_plan_with_nthreads(omp_get_max_threads());
  fftwf_plan plan_r2c = fftwf_plan_dft_r2c_2d(imgHeight, imgWidth, data, temp, FFTW_ESTIMATE);
  fftwf_plan plan_c2r = fftwf_plan_dft_c2r_2d(imgHeight, imgWidth, temp, data, FFTW_ESTIMATE);

  LIKWID_MARKER_REGISTER("fft");
  LIKWID_MARKER_START("fft");
  fftwf_execute_dft_r2c(plan_r2c, data, temp);

  float fact = 1.0 / imgSize;
  for (size_t i = 0; i != complexSize; ++i)
    reinterpret_cast<std::complex<float>*>(temp)[i] *= fact;

  fftwf_execute_dft_c2r(plan_c2r, temp, data);
  LIKWID_MARKER_STOP("fft");

  fftwf_free(temp);
  fftwf_destroy_plan(plan_r2c);
  fftwf_destroy_plan(plan_c2r);
  fftwf_cleanup_threads();
}

void fft2f_r2r_composite(int imgHeight, int imgWidth, float *data)
{
  const size_t imgSize = imgWidth * imgHeight;
  const size_t complexWidth = imgWidth / 2 + 1;
  const size_t complexSize = imgHeight * complexWidth;
  
  fftwf_complex* temp1 = reinterpret_cast<fftwf_complex*>(
      fftwf_malloc(complexSize * sizeof(fftwf_complex)));
  fftwf_complex* temp2 = reinterpret_cast<fftwf_complex*>(
      fftwf_malloc(complexSize * sizeof(fftwf_complex)));

  fft2f_r2c_composite(imgHeight, imgWidth, data, temp1, temp2);

  LIKWID_MARKER_START("fft-composite");
  float fact = 1.0 / imgSize;
  for (size_t i = 0; i != complexSize; ++i)
    reinterpret_cast<std::complex<float>*>(temp1)[i] *= fact;
  LIKWID_MARKER_STOP("fft-composite");

  fft2f_c2r_composite(imgHeight, imgWidth, temp1, data, temp2);

  fftwf_free(temp1);
  fftwf_free(temp2);
}

void test_r2r(int size)
{
  idg::Array2D<float> data_ref(size, size);
  init_data(data_ref);
  std::cout << "input" << std::endl;
  print(size, size, data_ref.data());
  
  // Reference
  std::cout << "fft_r2r" << std::endl;
  fft2f_r2r(size, size, data_ref.data());
  print(size, size, data_ref.data());
  print(size, size, data_ref.data());

  // Composite
  std::cout << "fft_r2r_composite" << std::endl;
  idg::Array2D<float> data_composite(size, size);
  init_data(data_composite);
  fft2f_r2r_composite(size, size, data_composite.data());
  print(size, size, data_composite.data());
  get_accuracy(data_ref, data_composite);
  std::cout << std::endl;
}

int main(int argc, char **argv)
{
  LIKWID_MARKER_INIT;

  test_r2r(SIZE);
  //test_r2r(SIZE*2);
  //test_r2r(SIZE+1);

  LIKWID_MARKER_CLOSE;
}
